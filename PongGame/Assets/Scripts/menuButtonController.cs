﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class menuButtonController : MonoBehaviour {
    gameControllerScript myGameController;
    // Use this for initialization
    void Start () {
        myGameController = GameObject.Find("GameController").GetComponent<gameControllerScript>();
        GetComponent<Button>().onClick.AddListener(() => loadMenu());
    }

    void loadMenu()
    {
        //the scene for the game
        SceneManager.LoadScene("menu");
        myGameController.player1Score = 0;
        myGameController.player2Score = 0;
    }
    // Update is called once per frame
    void Update () {
		
	}
}
