﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class gameControllerScript : MonoBehaviour {

   
    public int player1Score = 0, player2Score = 0, scoretoReach = 0;
    public bool gameEnded = false;

    void Start () {
        if(gameEnded == false)
        {
            DontDestroyOnLoad(this);
        }
        
    }
	
	
	void Update () {
        if((player1Score >= 10 || player2Score >= 10) && (SceneManager.GetActiveScene().name == "level1"))
        {
            SceneManager.LoadScene("level2");
            
        }

        else if ((player1Score >= 40 || player2Score >= 40) && (SceneManager.GetActiveScene().name == "level2"))
        {
            SceneManager.LoadScene("level3");

        }

        else if ((player1Score >= 100 || player2Score >= 100) && (SceneManager.GetActiveScene().name == "level3"))
        {
            SceneManager.LoadScene("scoresheet");
            

        }


    }
}
