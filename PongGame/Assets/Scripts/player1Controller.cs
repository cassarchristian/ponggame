﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player1Controller : MonoBehaviour {

	void Start () {
		
	}
    void Update () {

        float speed = 20.0f;

        float yMove = Input.GetAxis("Vertical") * Time.deltaTime * speed;
        transform.Translate(0f, yMove, 0f);
        // initially, the temporary vector should equal the player's position
        Vector3 clampedPosition = transform.position;
        // Now we can manipulte it to clamp the y element
        clampedPosition.y = Mathf.Clamp(transform.position.y, -3.40f, 3.40f);
        // re-assigning the transform's position will clamp it
        transform.position = clampedPosition;

    }
}
