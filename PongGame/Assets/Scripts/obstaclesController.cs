﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class obstaclesController : MonoBehaviour {

    float speed, xdirection, ydirection;
    float RotateSpeed = 2f, Radius = 1f, angle;
    Vector2 centre;
    // Use this for initialization
    void Start () {
        speed = Random.Range(1f, 2f);
        xdirection = Random.Range(-1f, 1f);
        ydirection = Random.Range(-1f, 1f);
        centre = transform.position;
    }

    // Update is called once per frame
    void Update () {
        angle += RotateSpeed * Time.deltaTime;
        var offset = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle)) * Radius;
        transform.position = centre + offset;
        transform.Rotate(Vector3.forward * speed * Time.deltaTime);
        transform.Translate(new Vector3(xdirection, ydirection) * speed * Time.deltaTime, Space.World);
        
    }
}
