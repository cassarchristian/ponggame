﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ballController : MonoBehaviour {

    Rigidbody2D rb;
    GameObject ball;
    gameControllerScript myGameController;
    Text player1points, player2points;
    public bool gameStarted = false;
    public int bounceCounter = 0, scoreIncrement = 2;
    List<float> directions = new List<float>(2);
    
    void Start () {
        ball = GameObject.Find("Ball");
        myGameController = GameObject.Find("GameController").GetComponent<gameControllerScript>();
        player1points = GameObject.Find("player1Score").GetComponent<Text>();
        player2points = GameObject.Find("player2Score").GetComponent<Text>();
        directions.Add(12.0f);
        directions.Add(-12.0f);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (SceneManager.GetActiveScene().name == "level1")
        {
            float randomDirection = Random.Range(-4f, 4f);
            bounceCounter++;
            if (bounceCounter == 1)
            {
                float randomY = Random.Range(-2f, 2f);
                GetComponent<Rigidbody2D>().velocity += new Vector2(0f, randomY);
                bounceCounter = 0;
            }

            if (collision.gameObject.tag == "p1base")
            {
                transform.position = new Vector3(0f, 0f, 0f);
                rb.velocity = new Vector2(-13f, randomDirection);
                myGameController.player2Score += scoreIncrement;
            }
            else if (collision.gameObject.tag == "p2base")
            {
                transform.position = new Vector3(0f, 0f, 0f);
                rb.velocity = new Vector2(13f, randomDirection);
                myGameController.player1Score += scoreIncrement;
            }
        }

        else if (SceneManager.GetActiveScene().name == "level2")
        {
            scoreIncrement = 5;
            float randomDirection = Random.Range(-4f, 4f);
            bounceCounter++;
            if (bounceCounter == 1)
            {
                float randomY = Random.Range(-2f, 2f);
                GetComponent<Rigidbody2D>().velocity += new Vector2(0f, randomY);
                bounceCounter = 0;
            }

            if (collision.gameObject.tag == "p1base")
            {
                transform.position = new Vector3(0f, 0f, 0f);
                rb.velocity = new Vector2(-14f, randomDirection);
                myGameController.player2Score += scoreIncrement;
            }
            else if (collision.gameObject.tag == "p2base")
            {
                transform.position = new Vector3(0f, 0f, 0f);
                rb.velocity = new Vector2(14f, randomDirection);
                myGameController.player1Score += scoreIncrement;
            }

            
        }

        else if (SceneManager.GetActiveScene().name == "level3")
        {
            scoreIncrement = 10;
            float randomDirection = Random.Range(-4f, 4f);
            bounceCounter++;
            if (bounceCounter == 1)
            {
                float randomY = Random.Range(-2f, 2f);
                GetComponent<Rigidbody2D>().velocity += new Vector2(0f, randomY);
                bounceCounter = 0;
            }

            if (collision.gameObject.tag == "p1base")
            {
                transform.position = new Vector3(0f, 0f, 0f);
                rb.velocity = new Vector2(-15f, randomDirection);
                myGameController.player2Score += scoreIncrement;
            }
            else if (collision.gameObject.tag == "p2base")
            {
                transform.position = new Vector3(0f, 0f, 0f);
                rb.velocity = new Vector2(15f, randomDirection);
                myGameController.player1Score += scoreIncrement;
            }
        }
        




    }

    void Update() {
        int randomStartDir = Random.Range(0, 2);
        if (gameStarted == false)
        {
            transform.position = new Vector3(0f,0f,0f);
        }


        if (Input.GetMouseButtonDown(0) && gameStarted == false)
        {
            gameStarted = true;
            float randomDirection = Random.Range(-4f, 4f);
            rb = GetComponent<Rigidbody2D>();
            rb.velocity = new Vector2(directions[randomStartDir], randomDirection);
        }        

        player1points.text = myGameController.player1Score.ToString();
        player2points.text = myGameController.player2Score.ToString();

    }


}
