﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class scoresheetController : MonoBehaviour {
    Text player1P, player2P, whoWon;
    gameControllerScript myGameController;
    // Use this for initialization
    void Start () {
        myGameController = GameObject.Find("GameController").GetComponent<gameControllerScript>();
        player1P = GameObject.Find("player1Score").GetComponent<Text>();
        player2P = GameObject.Find("player2Score").GetComponent<Text>();
        whoWon = GameObject.Find("playerWin").GetComponent<Text>();
        

    }

    

    // Update is called once per frame
    void Update () {

        player1P.text = "P l a y e r  1  S c o r e: " + myGameController.player1Score;
        player2P.text = "P l a y e r  2  S c o r e: " + myGameController.player2Score;

        if(myGameController.player1Score > myGameController.player2Score)
        {
            whoWon.text = "P l a y e r  1  w o n!";
        }
        else
        {
            whoWon.text = "P l a y e r  2  w o n!";
        }

    }
}
