﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class playButtonController : MonoBehaviour {
    

    // Use this for initialization
    void Start () {
        GetComponent<Button>().onClick.AddListener(() => loadGame());
    }


    void loadGame()
    {
        //the scene for the game#
        SceneManager.LoadScene("level1");
    }

    // Update is called once per frame
    void Update () {
		
	}
}
